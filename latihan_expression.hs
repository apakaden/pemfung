import Data.Maybe

data Expr = C Float | Expr :+ Expr | Expr :- Expr
          | Expr :* Expr | Expr :/ Expr 
          | V [Char]
          | Let String Expr Expr      
     deriving Show

subst :: String -> Expr -> Expr -> Expr

subst v0 e0 (V v1)          = if (v0 == v1) then e0 else (V v1)
subst v0 e0 (C c)           = (C c)
subst v0 e0 (e1 :+ e2)      = subst v0 e0 e1 :+ subst v0 e0 e2
subst v0 e0 (e1 :- e2)      = subst v0 e0 e1 :- subst v0 e0 e2
subst v0 e0 (e1 :* e2)      = subst v0 e0 e1 :* subst v0 e0 e2
subst v0 e0 (e1 :/ e2)      = subst v0 e0 e1 :/ subst v0 e0 e2
subst v0 e0 (Let v1 e1 e2)  = Let v1 e1 (subst v0 e0 e2)

evaluate :: Expr -> Float
evaluate (C x)          = x
evaluate (e1 :* e2)     = evaluate e1 * evaluate e2
evaluate (e1 :/ e2)     = evaluate e1 / evaluate e2
evaluate (e1 :+ e2)     = evaluate e1 + evaluate e2
evaluate (e1 :- e2)     = evaluate e1 - evaluate e2
evaluate (Let v e0 e1)  = evaluate (subst v e0 e1)
evaluate (V v)          = 0.0

test0 = Let "x" (C 5) (Let "y" (C 3) (V "x" :- V "y"))
test1 = (C 5) :+ (C 7) :- (C 2)

foldExpr (kali, bagi, tamb, kura, le, ve, con) (C c)    = con c
foldExpr (kali, bagi, tamb, kura, le, ve, con) (e1 :+ e2) = tamb (foldExpr (kali, bagi, tamb, kura, le, ve, con) e1) (foldExpr (kali, bagi, tamb, kura, le, ve, con) e2)
foldExpr (kali, bagi, tamb, kura, le, ve, con) (e1 :- e2) = kura (foldExpr (kali, bagi, tamb, kura, le, ve, con) e1) (foldExpr (kali, bagi, tamb, kura, le, ve, con) e2)
foldExpr (kali, bagi, tamb, kura, le, ve, con) (e1 :* e2) = kali (foldExpr (kali, bagi, tamb, kura, le, ve, con) e1) (foldExpr (kali, bagi, tamb, kura, le, ve, con) e2)
foldExpr (kali, bagi, tamb, kura, le, ve, con) (e1 :/ e2) = tamb (foldExpr (kali, bagi, tamb, kura, le, ve, con) e1) (foldExpr (kali, bagi, tamb, kura, le, ve, con) e2)
foldExpr (kali, bagi, tamb, kura, le, ve, con) (Let v e0 e1) = le v e0 e1
foldExpr (kali, bagi, tamb, kura, le, ve, con) (V v) = ve v

newEvaluate = foldExpr (kali, bagi, tamb, kura, le, ve, con)
    where
        kali = (*)
        bagi = (/)
        tamb = (+)
        kura = (-)
        le v e0 e1 = newEvaluate (subst v e0 e1)
        ve v = 0.0
        con c = c

countConstant = foldExpr (kali, bagi, tamb, kura, le, ve, con)
    where
        kali = (+)
        bagi = (+)
        tamb = (+)
        kura = (+)
        le v e0 e1 = countConstant (subst v e0 e1)
        ve v = 0.0
        con _ = 1

countVariable = foldExpr (kali, bagi, tamb, kura, le, ve, con)
    where
        kali = (+)
        bagi = (+)
        tamb = (+)
        kura = (+)
        le v e0 e1 = 1 + countVariable (subst v e0 e1)
        ve v = 0.0
        con _ = 0.0

isDivZero = foldExpr (kali, bagi, tamb, kura, le, ve, con)
    where
        kali (Just x) (Just y) = Just (x * y)
        kali _ _ = Nothing
        bagi (Just x) (Just y) = if (y == 0.0) then Nothing else (Just (x / y))
        bagi _ _ = Nothing
        tamb (Just x) (Just y) = Just (x + y)
        tamb _ _ = Nothing
        kura (Just x) (Just y) = Just (x - y)
        kura _ _ = Nothing
        le v e0 e1 = subst v e0 e1
        ve v = Just 0.0
        con c = Just c
