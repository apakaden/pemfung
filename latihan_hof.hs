import Data.Maybe

length' :: [a] -> Int
length' x = sum(map toOne x)
    where toOne _ = 1

no2 xs = map (+1) (map (+1) xs)


iter :: Int -> (a -> a) -> a -> a
iter 0 f x = x
iter n f x = f(iter (n-1) f x)

sumSquare :: Int -> Int
sumSquare n = foldr (+) 0 (map (\x -> x * x) [1..n])

mystery xs = (map sing xs)
    where
        sing x = [x]

flip' :: (a -> b -> c) -> b -> a -> c
-- beda sama yang pdf
flip' a b c = a c b

no211 :: [Int] -> [Int]
no211 xs = map (+1) xs

no212 :: [Int] -> [Int] -> [Int]
no212 xs ys = concat (map (\x -> map (\y -> x + y) ys ) xs)

no212b :: [Int] -> [Int] -> [(Int, Int)]
no212b xs ys = concat (map (\x -> map (\y -> (x, y)) ys ) xs)

no213 :: [Int] -> [Int]
no213 xs = map (\x -> x + 2) (filter (\x -> x > 3) xs)

no214 :: [(Int, a)] -> [Int]
no214 xys = map (\(x, _) -> x + 3) xys

no215 :: [(Int, Int)] -> [Int]
no215 xys = map (\(x, _) -> x + 4) (filter (\(x, y) -> x + y < 5) xys)

no216 :: [Maybe Int] -> [Int]
no216 mxs = map (\(Just x) -> x + 5) (filter isJust' mxs)

no221 :: [Int] -> [Int]
no221 xs = [ x+3 | x <- xs ]

no222 :: [Int] -> [Int]
no222 xs = [ x | x <- xs, x > 7]

no223 :: [Int] -> [Int] -> [(Int, Int)]
no223 xs ys = [ (x, y) | x <- xs, y <- ys ]

no224 :: [(Int, Int)] -> [Int]
no224 xys = [ x + y | (x, y) <- xys, x + y > 3 ]

map' f [] = []
map' f (x:xs) = f x : map' f xs

max' a b
    | a > b = a
    | otherwise = b

maximum' [x] = x
maximum' (x:xs) = max' x (maximum' xs)

repeat' x = x:repeat' x

take' n _
    | n <= 0 = []

take' _ [] = []
take' n (x:xs) = x:take' (n-1) xs

isJust' :: Maybe a -> Bool
isJust' (Just x) = True
isJust' _ = False

