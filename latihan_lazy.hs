import Data.List

divisor n = [ i | i <- [1..n], n `mod` i == 0 ]

fpb a b = maximum (filter (\x -> b `mod` x == 0) (divisor a))
kpk a b = (a * b) `div` (fpb a b)

quicksort [] = []
quicksort (x:xs) = quicksort [ i | i <- xs, i <= x ]
                    ++ [x] ++
                    quicksort [ i | i <- xs, i > x ]

merge xs [] = xs
merge [] ys = ys
merge (x:xs) (y:ys)
    | x < y = x : merge xs (y:ys)
    | otherwise = y : merge (x:xs) ys

mergesort [] = []
mergesort [x] = [x]
mergesort xs = merge a b
    where
        n = length xs `div` 2
        a = mergesort (take n xs)
        b = mergesort (drop n xs)

permutation [] = [[]]
permutation ls = [ x:ps | x <- ls, ps <- permutation (ls\\[x]) ]

primes = sieve [2..]
    where sieve (x:xs) = x:[ y | y <- sieve xs, y `mod` x /= 0 ]

triplePythagoras = [ (x, y, z) | z <- [1..], y <- [1..z], x <- [1..y], z * z == y * y + x * x ]

splits []         = [([],[])]
splits (x:xs)     = ([],x:xs) : [(x:ps,qs) | (ps,qs)<-splits xs]

add' (x:xs) (y:ys) = (x+y) : add' xs ys
fib = 0 : 1 : add' fib (tail fib)
fib' = 0 : 1 : zipWith (+) fib (tail fib)

fac' = 1 : 1 : zipWith (*) [2..] (tail fac')
