data Tree a = Leaf a | Branch (Tree a) (Tree a)

treeSize :: Tree a -> Integer
treeSize (Leaf _) = 1
treeSize (Branch t1 t2) = treeSize t1 + treeSize t2

treeHeight :: Tree a -> Integer
treeHeight (Leaf _) = 0
treeHeight (Branch t1 t2) = 1 + max (treeHeight t1) (treeHeight t2)

data InternalTree a = ILeaf | IBranch a (InternalTree a) (InternalTree a)

preOrder :: InternalTree a -> [a]
preOrder (ILeaf) = []
preOrder (IBranch x t1 t2) = [x] ++ (preOrder t1) ++ (preOrder t2)

inOrder :: InternalTree a -> [a]
inOrder (ILeaf) = []
inOrder (IBranch x t1 t2) = (preOrder t1) ++ [x] ++ (preOrder t2)

mapInternalTree :: ( a -> b ) -> InternalTree a -> InternalTree b
mapInternalTree _ (ILeaf) = ILeaf
mapInternalTree f (IBranch a t1 t2) = IBranch (f a) (mapInternalTree f t1) (mapInternalTree f t2)

foldInternalTreeCenter :: (a -> b -> b) -> b -> InternalTree a -> b
foldInternalTreeCenter _ base (ILeaf) = base
foldInternalTreeCenter f base (IBranch a t1 t2) = foldInternalTreeCenter f (f a (foldInternalTreeCenter f base t1)) t2

foldInternalTreel :: (a -> b -> b) -> b -> InternalTree a -> b
foldInternalTreel _ base (ILeaf) = base
foldInternalTreel f base (IBranch a t1 t2) = foldInternalTreel f (foldInternalTreel f (f a base) t1) t2


numsInternalTree = IBranch 10 (IBranch 20 (ILeaf) (ILeaf)) (IBranch 30 (ILeaf) (ILeaf))

data BST a = Empty | Node a (BST a) (BST a) deriving (Show)

newNode :: a -> BST a
newNode x = Node x Empty Empty

bstInsert :: (Ord a) => a -> BST a -> BST a
bstInsert x Empty = newNode x
bstInsert x (Node y left right)
    | x == y = Node y left right
    | x < y = Node y (bstInsert x left) right
    | x > y = Node y left (bstInsert x right)

nums = [8, 6, 4, 1, 7, 3, 5]
numsBst = foldr bstInsert Empty nums
